import { AppService } from './app.service';
import { Component } from '@angular/core';
import { Contato } from './contato';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'tables';
  private contatos:Contato[] = [];
  private msg: string;

  constructor(private AppService : AppService) {            
  }


  ngOnInit() {
    this.carregarListaTodosContatos()
  }
  carregarListaTodosContatos() {
    
    this.AppService.getContatos().subscribe(
      contatos => {
        this.contatos = contatos           
      },
      error => {
        console.log(error);
      });
      ;
  }

  getAlterar(row: any){
    console.log(row)
    return true;
  }
  getExcluir(row: any){
    console.log(row) 
    this.contatos.splice(this.contatos.indexOf(row), 1);
    console.log(this.contatos);

  }
  addContato(){

    const donaDin: Contato = {
      nome: "Wannessa Karollyne da Silva",
      email: "kwannessa@hotmail.com",
      phone: "(81) 99810-1080",
      funcao: "Sorrir pá Pedinho",
      oc: false
    }

    this.contatos.push(donaDin)
  }
}



