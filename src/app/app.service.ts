import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, range } from 'rxjs';
import { Contato } from './contato';

@Injectable({
  providedIn: 'root'
})
export class AppService {

  constructor(private http:HttpClient) { }

  private baseUrl:string = 'http://demo3803018.mockable.io'
  private metodo:string = '/listar'
  private contatos:string = '/contatos'
  

  getContatos() : Observable<Contato[]>{
    return this.http.get<Contato[]>(this.baseUrl + this.metodo + this.contatos)

  }

  // mockContatos(){
  //   let numbers = [1, 2, 3];   
  //   let c: Contato;
  //   c.nome = 'Pedro'
  //   c.email = "pedro@pedro"
  //   c.funcao = 'Develop'
  //   c.oc = true
  //   c.phone = "(81) 99999-9999"
  //   let contatos: Contato[];    
  //   for (let num of numbers){
  //     contatos.push(c)        
  //   }

  //   return contatos

  // }

}
